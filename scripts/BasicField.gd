tool

extends ColorRect


onready var label = $Container/Label
onready var field = $Container/Value

const TYPE = "BasicField"

export(String) var text: String setget _set_text
var value: String setget _set_value
var editable: bool = false


func _ready() -> void:
	label.text = text


func has_value() -> bool:
	return value != ""


func disable_editable():
	_set_editable(false)


func switch_editable():
	_set_editable(!editable)


func _set_editable(value: bool) -> void:
	editable = value
	field.editable = value
	field.set_virtual_keyboard_enabled(value)


func _set_text(new_value):
	text = new_value
	if label != null:
		label.text = text


func _set_value(new_value) -> void:
	value = new_value
	field.text = new_value


func _on_Value_text_changed(new_text: String) -> void:
	value = new_text


func _on_Value_focus_entered() -> void:
	pass
