# Line
extends Control

var person: Person
var touched: bool = false


func init(person: Person) -> void:
	self.person = person
	
	var label = $Panel/LabelColor
	var forename = $Panel/ForenameLbl
	var location = $Panel/AdditionalInfo/LocationLbl
	var date = $Panel/AdditionalInfo/DateTime/DateLbl
	var time = $Panel/AdditionalInfo/DateTime/TimeLbl
	
	forename.text = person.forename
	location.text = person.location
	date.text = person.date
	time.text = person.time
	
	match(person.label):
		"GREEN":
			label.color = Global.GREEN
		"YELLOW":
			label.color = Global.YELLOW
		"RED":
			label.color = Global.RED
		"GREY":
			label.color = Global.GREY
		_:
			label.color = Global.WHITE
	
	if person.special:
		forename.modulate = Global.GOLD
	else:
		forename.modulate = Global.WHITE
	
	if forename.text == "":
		forename.text = "?"
	
	if location.text == "":
		location.text = "?"


func get_person() -> Person:
	return person


func remove() -> void:
	self.queue_free()


func _on_Line_gui_input(event: InputEvent) -> void:
	if event is InputEventScreenDrag:
		touched = false
	if event is InputEventScreenTouch:
		if event.is_pressed():
			touched = true
			yield(get_tree().create_timer(0.20),"timeout")
			touched = false
		if not event.is_pressed():
			if touched:
				touched = false
				EventMgt.emit_signal("switch_to_person_card_panel", self)
