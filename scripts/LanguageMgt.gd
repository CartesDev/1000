extends Node


enum { EN, PL }


func translate(lang_code: int) -> void:
	match(lang_code):
		EN:
			for node in get_tree().get_nodes_in_group("translation"):
				node.translation(EN)
		PL:
			
			for node in get_tree().get_nodes_in_group("translation"):
				node.translation(PL)

