extends ColorRect


enum Func { ALL, GREEN, YELLOW, RED, GREY, SPECIAL, CONTACT }

var person_list = DataMgt.person_list
var last_used_func: int = Func.ALL


func _init() -> void:
	EventMgt.connect("sort_again_with_last_sorting_fliter", self, "_sort_again_with_last_sorting_fliter")


func _ready() -> void:
	last_used_func = Func.ALL
	_select_button($Panel/SortAllBtn)


# disables selceted button
func _select_button(selected_button: Button) -> void:
	var buttons = $Panel.get_children()
	for button in buttons:
		button.disabled =  button == selected_button


func _sort_by_label_color(color: String):
	var sorted_person_list: Array
	for person in person_list:
		if person.label == color:
			sorted_person_list.append(person)
	EventMgt.emit_signal("sort_lines", sorted_person_list)


func _on_SortAllBtn_pressed() -> void:
	EventMgt.emit_signal("sort_lines", person_list)
	last_used_func  = Func.ALL
	_select_button($Panel/SortAllBtn)


func _on_SortGreenBtn_pressed() -> void:
	_sort_by_label_color("GREEN")
	last_used_func  = Func.GREEN
	_select_button($Panel/SortGreenBtn)


func _on_SortYellowBtn_pressed() -> void:
	_sort_by_label_color("YELLOW")
	last_used_func  = Func.YELLOW
	_select_button($Panel/SortYellowBtn)


func _on_SortRedBtn_pressed() -> void:
	_sort_by_label_color("RED")
	last_used_func  = Func.RED
	_select_button($Panel/SortRedBtn)


func _on_SortGreyBtn_pressed() -> void:
	_sort_by_label_color("GREY")
	last_used_func  = Func.GREY
	_select_button($Panel/SortGreyBtn)


func _on_SortSpecialBtn_pressed() -> void:
	var sorted_person_list: Array
	for person in person_list:
		if person.special == true:
			sorted_person_list.append(person)
	EventMgt.emit_signal("sort_lines", sorted_person_list)
	last_used_func  = Func.SPECIAL
	_select_button($Panel/SortSpecialBtn)


func _on_SortContactBtn_pressed() -> void:
	var sorted_person_list: Array
	for person in person_list:
		if person.contact != "":
			sorted_person_list.append(person)
	EventMgt.emit_signal("sort_lines", sorted_person_list)
	last_used_func  = Func.CONTACT
	_select_button($Panel/SortContactBtn)


# calls last used function
# / unfortunately FuncRef and call() doesn't work on android /
func _sort_again_with_last_sorting_fliter() -> void:
	match(last_used_func):
		Func.ALL:
			_on_SortAllBtn_pressed()
		Func.GREEN:
			_on_SortGreenBtn_pressed()
		Func.YELLOW:
			_on_SortYellowBtn_pressed()
		Func.RED:
			_on_SortRedBtn_pressed()
		Func.GREY:
			_on_SortGreyBtn_pressed()
		Func.SPECIAL:
			_on_SortSpecialBtn_pressed()
		Func.CONTACT:
			_on_SortContactBtn_pressed()
