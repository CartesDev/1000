extends Control


const ANIM_DURATION = 0.36

onready var general_panel = $Panels/GeneralPanel
onready var new_record_panel = $Panels/NewRecordPanel
onready var person_card_panel = $Panels/PersonCardPanel
onready var settings_panel = $Panels/SettingsPanel

var current_panel = null


func _init() -> void:
	EventMgt.connect("switch_to_main_panel", self, "_switch_to_main_panel")
	EventMgt.connect("switch_to_new_record_panel", self, "_switch_to_new_record_panel")
	EventMgt.connect("switch_to_person_card_panel", self, "_switch_to_person_card_panel")
	EventMgt.connect("switch_to_settings_panel", self, "_switch_to_settings_panel")


func _ready() -> void:
	current_panel = general_panel


func _switch_to_new_record_panel() -> void:
	new_record_panel.init()
	_anim(new_record_panel)
	yield(get_tree().create_timer(ANIM_DURATION), "timeout")
	_switch_to_panel(new_record_panel)


func _switch_to_main_panel() -> void:
	_anim(general_panel, true)
	yield(get_tree().create_timer(ANIM_DURATION), "timeout")
	_switch_to_panel(general_panel)


func _switch_to_person_card_panel(line) -> void:
	var lines = general_panel.get_lines()
	person_card_panel.init(line, lines)
	_anim(person_card_panel)
	yield(get_tree().create_timer(ANIM_DURATION), "timeout")
	_switch_to_panel(person_card_panel)


func _switch_to_settings_panel() -> void:
	settings_panel.init()
	_anim(settings_panel)
	yield(get_tree().create_timer(ANIM_DURATION), "timeout")
	_switch_to_panel(settings_panel)


# switch to panel function
func _switch_to_panel(current_panel) -> void:
	var panel_list = $Panels
	for panel in panel_list.get_children():
		panel.visible = current_panel == panel


# animates panel switching
func _anim(panel, reverse = false) -> void:
	var init_value = Vector2.ZERO - Vector2(Global.SCREEN_SIZE.x, 0)
	panel.visible = true
	
	if reverse:
		$Tween.interpolate_property(current_panel, "rect_position", Vector2.ZERO, init_value, ANIM_DURATION)
		$Tween.interpolate_property(current_panel, "rect_scale", Vector2.ONE, Vector2(0.8, 0.8), ANIM_DURATION)
		$Tween.interpolate_property(general_panel, "rect_scale", Vector2(0.8, 0.8), Vector2.ONE, ANIM_DURATION)
		$Tween.interpolate_property(general_panel, "modulate:a", 0, 1, ANIM_DURATION)
	else:
		$Tween.interpolate_property(panel, "rect_position", init_value, Vector2.ZERO, ANIM_DURATION)
		$Tween.interpolate_property(panel, "rect_scale", Vector2(0.8, 0.8), Vector2.ONE, ANIM_DURATION)
		$Tween.interpolate_property(general_panel, "rect_scale", Vector2.ONE, Vector2(0.8, 0.8), ANIM_DURATION)
		$Tween.interpolate_property(general_panel, "modulate:a", 1, 0, ANIM_DURATION)
	
	$Tween.start()
	
	current_panel = panel
