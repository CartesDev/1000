extends ColorRect


func init(text: String) -> void:
	$LineEdit.text = text


func get_text() -> String:
	return $LineEdit.text


func get_line() -> Node:
	return $LineEdit


func _on_LineEdit_focus_exited() -> void:
	EventMgt.emit_signal("add_category", self)

