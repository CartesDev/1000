extends ColorRect


func _init() -> void:
	EventMgt.connect("set_stats", self, "_set_stats")


func _set_stats(person_list):
	var actual_amount: float = 0
	var green_amount = 0
	var yellow_amount = 0
	var red_amount = 0
	var grey_amount = 0
	
	var contact_amount = 0
	var contact_proc = 0.0
	
	for i in person_list.size():
		var color = person_list[i].label
		
		actual_amount += 1
		
		if person_list[i].contact != "":
			contact_amount += 1
		
		match(color):
			"GREEN":
				green_amount += 1
			"YELLOW":
				yellow_amount += 1
			"RED":
				red_amount += 1
			"GREY":
				grey_amount += 1
	
	if green_amount == 0:
		green_amount = "?"
	
	if yellow_amount == 0:
		yellow_amount = "?"
	
	if red_amount == 0:
		red_amount = "?"
	
	if grey_amount == 0:
		grey_amount = "?"
	
	if actual_amount != 0:
		contact_proc = float(contact_amount / actual_amount) * 100
	else:
		contact_proc = "?"
	
	var amount_stats = $Contaier/Stats/Amount/
	var curr_amout_lbl = $Contaier/CurrAmount
	var green_lbl = amount_stats.get_node("GreenLbl")
	var yellow_lbl = amount_stats.get_node("YellowLbl")
	var red_lbl = amount_stats.get_node("RedLbl")
	var grey_lbl = amount_stats.get_node("GreyLbl")
	var contact_proc_lbl = $Contaier/Stats/Proc/ContactProc
	
	curr_amout_lbl.text = str(int(actual_amount), "/", "1000")
	
	green_lbl.self_modulate = Global.GREEN
	green_lbl.text = str(green_amount)
	
	yellow_lbl.self_modulate = Global.YELLOW
	yellow_lbl.text = str(yellow_amount)
	
	red_lbl.self_modulate = Global.RED
	red_lbl.text = str(red_amount)
	
	grey_lbl.self_modulate = Global.GREY
	grey_lbl.text = str(grey_amount)
	
	if str(contact_proc) != "?":
		contact_proc_lbl.text = str(contact_proc).pad_decimals(2) + "%"
	else:
		contact_proc_lbl.text = "?%"
