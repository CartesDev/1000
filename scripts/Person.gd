class_name Person

extends Node


const DOT := "."
const COLON := ":"

var forename: String
var location: String
var contact: String
var date: String
var time: String
var category: String
var description: String
var label: String
var special: bool

func _init(forename: String, location: String, contact: String, category: String, 
		description: String, label: String, special: bool, date := "", time := "") -> void:
	self.forename = forename
	self.location = location
	self.contact = contact
	self.category = category
	self.description = description
	self.label = label
	self.special = special
	
	if date != "":
		self.date = date
	else:
		self.date = _get_date()
	
	if time != "":
		self.time = time
	else:
		self.time = _get_time()


func _get_date() -> String:
	var date = OS.get_date()
	return str(date.day, DOT, date.month, DOT, date.year)


func _get_time() -> String:
	var time = OS.get_time()
	
	if int(time.minute) < 10:
		time.minute = "0" + str(time.minute)
	
	if int(time.hour) < 10:
		time.hour = "0" + str(time.hour)
	
	return str(time.hour, COLON, time.minute)


func get_data_dict() -> Dictionary:
	var data = {}
	
	data.forename = forename
	data.location = location
	data.date = date
	data.time = time
	data.category = category
	data.contact = contact
	data.description = description
	data.label = label
	data.special = special
	
	return data
