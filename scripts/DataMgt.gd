extends Node


const SERIAL_ID := "SAJ31DSJD341"
const USER_DIR := "user://save.data"

var person_list: Array
var category_list: Array
var location_list: Array
var notes: String
var language: int


func _ready() -> void:
	load_data()


func load_data() -> void:
	var file = File.new()
	var err = file.open(USER_DIR, File.READ)
	if err != OK:
		save_data()
		load_data()
	if err == OK:
		var data = parse_json(file.get_as_text())
		_load_data(data)
		file.close()


func save_data() -> void:
	var data = _save_data()
	var file = File.new()
	file.open(USER_DIR, File.WRITE)
	file.store_line(to_json(data))
	file.close()


func _load_data(data_dir) -> void:
	var data: Dictionary = data_dir[SERIAL_ID]
	
	if data.empty():
		return
	
	for i in data.person_list.size():
		var person = data.person_list[i]
		create_person(person.forename, person.location, person.contact, person.category, 
				person.description, person.label, person.special, person.date, person.time)
	
	for i in data.category_list.size():
		var category_text = data.category_list[i]
		create_category(category_text, true)
	
	for i in data.location_list.size():
		var location_text = data.location_list[i]
		create_location(location_text, true)
	
	notes = data.notes
	language = data.language


func _save_data() -> Dictionary:
	var data_dir = {}
	var data = {}
	
	var person_list_arr = []
	for i in person_list.size():
		person_list_arr.append(person_list[i].get_data_dict())
	data.person_list = person_list_arr
	
	var category_list_arr = []
	for i in category_list.size():
		category_list_arr.append(category_list[i].get_text())
	data.category_list = category_list_arr
	
	var location_list_arr = []
	for i in location_list.size():
		location_list_arr.append(location_list[i].get_text())
	data.location_list = location_list_arr
	
	data.notes = notes
	data.language = language
	
	data_dir[SERIAL_ID] = data
	
	return data_dir


func create_person(forename, location, contact, category, description, label, special, 
		date = "", time = "") -> Person:
	var person = Person.new(
			forename, location, contact, 
			category, description, label, 
			special, date, time)
	person_list.append(person)
	
	return person 


func remove_person(person: Person):
	person_list.erase(person)


func create_category(category, initialize = false) -> void:
	if initialize:
		var category_text = category
		category = Global.CATEGORY.instance()
		category.init(category_text)
	
	category_list.append(category)


func remove_category(category) -> void:
	category_list.erase(category)


func create_location(location, initialize = false) -> void:
	if initialize:
		var location_text = location
		location = Global.LOCATION.instance()
		location.init(location_text)
		
	location_list.append(location)


func remove_location(location) -> void:
	location_list.erase(location)


func set_language(lang_code) -> void:
	language = lang_code
