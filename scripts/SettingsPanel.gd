#SettingsPanel
extends ColorRect


const en_texture = preload("res://res/en.tres")
const pl_texture = preload("res://res/pl.tres")

onready var category_list = $Panel/CategoryList
onready var location_list = $Panel/LocationList
onready var notes_list = $Panel/NotesList
onready var location_button = $Panel/Tabs/LocationButton
onready var category_button = $Panel/Tabs/CategoryButton
onready var notes_button = $Panel/Tabs/NotesButton
onready var lang_button = $Panel/Header/LanguageButton


func _init() -> void:
	EventMgt.connect("add_category", self, "_add_category")
	EventMgt.connect("add_location", self, "_add_location")
	EventMgt.connect("add_notes", self, "_add_notes")


func _ready() -> void:
	_on_CategoryButton_pressed()
	LanguageMgt.translate(DataMgt.language)
	_set_button_texture(DataMgt.language)


# initializes location and category data from DataMgt
func init() -> void:
	# restore category data
	var categories = category_list.get_children()
	for category in DataMgt.category_list:
		if not categories.has(category):
			category_list.add_child(category)
	
	# restore location data
	var locations = location_list.get_children()
	for location in DataMgt.location_list:
		if not locations.has(location):
			location_list.add_child(location)
	
	# create new empty category if empty field doesn't exist
	if categories.empty():
		var category = Global.CATEGORY.instance()
		category_list.add_child(category)
	elif categories[-1].get_text() != "":
		var category = Global.CATEGORY.instance()
		category_list.add_child(category)

	# create new empty location if empty field doesn't exist
	if locations.empty():
		var location = Global.LOCATION.instance()
		location_list.add_child(location)
	elif locations[-1].get_text() != "":
		var location = Global.LOCATION.instance()
		location_list.add_child(location)
	
	# restore notes data
	notes_list.text = DataMgt.notes
	
	DataMgt.save_data()


# removes all empty lines excluding the last "empty" line
func _remove_unused_categories() -> void:
	if category_list.get_child_count() > 1:
		var category_list_tmp: Array = category_list.get_children()
		category_list_tmp.remove(category_list_tmp.size() - 1)
		for category in category_list_tmp:
			if category.get_text() == "":
				DataMgt.remove_category(category)
				DataMgt.save_data()
				category.queue_free()


# removes all empty lines excluding the last "empty" line
func _remove_unused_locations() -> void:
	if location_list.get_child_count() > 1:
		var location_list_tmp: Array = location_list.get_children()
		location_list_tmp.remove(location_list_tmp.size() - 1)
		for location in location_list_tmp:
			if location.get_text() == "":
				DataMgt.remove_location(location)
				DataMgt.save_data()
				location.queue_free()


# switch section function
func _switch_section(section) -> void:
	category_button.disabled = false
	category_list.visible = false
	location_button.disabled = false
	location_list.visible = false
	notes_button.disabled = false
	notes_list.visible = false
	
	match(section):
		"CATEGORY":
			category_button.disabled = true
			category_list.visible = true
		"LOCATION":
			location_button.disabled = true
			location_list.visible = true
		"NOTES":
			notes_button.disabled = true
			notes_list.visible = true


# sets button texture
func _set_button_texture(lang_code) -> void:
	match(lang_code):
		LanguageMgt.EN:
			lang_button.set("custom_styles/hover", en_texture)
			lang_button.set("custom_styles/pressed", en_texture)
			lang_button.set("custom_styles/focus", en_texture)
			lang_button.set("custom_styles/disabled", en_texture)
			lang_button.set("custom_styles/normal", en_texture)
		LanguageMgt.PL:
			lang_button.set("custom_styles/hover", pl_texture)
			lang_button.set("custom_styles/pressed", pl_texture)
			lang_button.set("custom_styles/focus", pl_texture)
			lang_button.set("custom_styles/disabled", pl_texture)
			lang_button.set("custom_styles/normal", pl_texture)


# [EVENT] add new category
func _add_category(new_category) -> void:
	# skips if the location already exist or is empty, otherwise creates a new category data
	if not DataMgt.category_list.has(new_category) and new_category.get_text() != "":
		DataMgt.create_category(new_category)
	
	_remove_unused_categories()
	
	# checks if the last line is empty, if not creates a new "empty" line
	if category_list.get_child_count() > 0:
		var last_category = category_list.get_children()[-1]
		if last_category.get_text() != "":
			var category = Global.CATEGORY.instance()
			category_list.add_child(category)
	
	DataMgt.save_data()


# [EVENT] add a new location
func _add_location(new_location) -> void:
	# skips if the location already exist or is empty, otherwise creates a new location data
	if not DataMgt.location_list.has(new_location) and new_location.get_text() != "":
		DataMgt.create_location(new_location)
	
	_remove_unused_locations()
	
	# checks if the last line is empty, if not creates a new "empty" line
	if location_list.get_child_count() > 0:
		var last_location = location_list.get_children()[-1]
		if last_location.get_text() != "":
			var location = Global.LOCATION.instance()
			location_list.add_child(location)
	
	DataMgt.save_data()


# [EVENT] switches to category section
func _on_CategoryButton_pressed() -> void:
	_switch_section("CATEGORY")


# [EVENT] switches to location section
func _on_LocationButton_pressed() -> void:
	_switch_section("LOCATION")


# [EVENT] switches to notes section
func _on_NotesButton_pressed() -> void:
	_switch_section("NOTES")


# [EVENT] switches to main panel
func _on_Button_pressed() -> void:
	EventMgt.emit_signal("switch_to_main_panel")


# [EVENT] saves notes on text changes
func _on_NotesList_text_changed() -> void:
	DataMgt.notes = notes_list.text
	DataMgt.save_data()


# [EVENT] releases focus on background click
func _on_SettingsPanel_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.is_pressed():
		var current_focus_control = get_focus_owner()
		if current_focus_control:
			current_focus_control.release_focus()


# [EVENT] 
func _on_LanguageButton_pressed() -> void:
	if DataMgt.language != LanguageMgt.EN:
		DataMgt.set_language(LanguageMgt.EN)
		LanguageMgt.translate(LanguageMgt.EN)
		_set_button_texture(LanguageMgt.EN)
	else:
		DataMgt.set_language(LanguageMgt.PL)
		LanguageMgt.translate(LanguageMgt.PL)
		_set_button_texture(LanguageMgt.PL)


func translation(lang_code: int) -> void:
	var header = $Panel/Header/Label
	var category = category_button
	var location = location_button
	var notes = notes_button
	var back = $Panel/Button
	
	match(lang_code):
		LanguageMgt.EN:
			header.text = "SETTINGS"
			location.text = "Locations"
			category.text = "Categories"
			notes.text = "Notes"
			back.text = "Back"
		LanguageMgt.PL:
			header.text = "USTAWIENIA"
			location.text = "Lokalizacje"
			category.text = "Kategorie"
			notes.text = "Notatki"
			back.text = "Wróć"
