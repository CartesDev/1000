extends ColorRect


var value: String setget _set_value


# set value function
func _set_value(new_value) -> void:
	value = new_value
	match(new_value):
		"GREEN":
			color = Global.GREEN
		"YELLOW":
			color = Global.YELLOW
		"RED":
			color = Global.RED
		"GREY":
			color = Global.GREY
		_:
			color = Global.WHITE
