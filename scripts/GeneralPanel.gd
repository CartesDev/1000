extends Control


onready var lines = $Panel/Scroll/Lines
onready var sort_panel = $Panel/SortPanel


func _init() -> void:
	EventMgt.connect("create_line", self, "_create_line")
	EventMgt.connect("sort_lines", self, "_sort_lines")


func _ready() -> void:
	EventMgt.emit_signal("set_stats", DataMgt.person_list)
	_restore_lines()


# gets lines
func get_lines() -> Array:
	return lines.get_children()


# creates new line
func _create_line(person, sort_again = true) -> void:
	var line = Global.LINE.instance()
	line.init(person)
	lines.add_child(line)
	if sort_again:
		EventMgt.emit_signal("sort_again_with_last_sorting_fliter")


func _remove_lines() -> void:
	for line in lines.get_children():
		line.queue_free()


# restores lines
func _restore_lines() -> void:
	for i in DataMgt.person_list.size():
		_create_line(DataMgt.person_list[i])


# [EVENT]
func _sort_lines(sorted_person_list: Array):
	_remove_lines()
	for sorted_person in sorted_person_list:
		_create_line(sorted_person, false)


# [EVENT] switch to new record panel
func _on_AddButton_pressed() -> void:
	EventMgt.emit_signal("switch_to_new_record_panel")


# [EVENT] switch to settings panel
func _on_SettingsButton_pressed() -> void:
	EventMgt.emit_signal("switch_to_settings_panel")
