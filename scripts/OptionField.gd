tool

extends ColorRect


onready var label = $Container/Label
onready var field = $Container/Value

const TYPE = "OptionField"

export(String) var text: String setget _set_text
var value: String setget _set_value
var editable: bool = false
var id: int = 0

func _ready() -> void:
	label.text = text


# inits items to option field
func init(items, value) -> void:
	id = 0
	field.clear()
	field.add_item("")
	
	for item in items:
		field.add_item(item.get_text())
		if item.get_text() == value:
			field.select(id)
		id+=1
	
	if get_item_count() == 1:
		disable()
	else:
		enable()


# checks if values is not empty
func has_value() -> bool:
	return value != ""


# gets item count
func get_item_count() -> int:
	return field.get_item_count()


func enable() -> void:
	field.disabled = false


func disable() -> void:
	field.disabled = true


# sets field editable to false
func disable_editable():
	_set_editable(true)


func switch_editable():
	_set_editable(!editable)


func _set_editable(value: bool) -> void:
	if get_item_count() == 1:
		disable()
		return
	editable = value
	field.disabled = value


func _set_text(new_value):
	text = new_value
	if label != null:
		label.text = text


func _set_value(new_value) -> void:
	value = new_value
	field.text = new_value


func _on_Value_item_selected(index: int) -> void:
	value = field.get_item_text(index)
