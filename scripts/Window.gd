extends PopupDialog


func _on_YesButton_pressed() -> void:
	EventMgt.emit_signal("remove_person")
	visible = false


func _on_NoButton_pressed() -> void:
	visible = false

