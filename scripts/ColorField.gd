tool

extends ColorRect

onready var label = $Container/Header/Label
onready var color_desc = $Container/Header/ColorDesc

var green = "GREEN"
var yellow = "YELLOW"
var red = "RED"
var grey = "GREY"

export(String) var text: String setget _set_text

var value: String = "" setget _set_value


func _ready() -> void:
	label.text = text


func _set_text(new_value):
	text = new_value
	if label != null:
		label.text = new_value


func _set_value(new_value) -> void:
	value = new_value
	_set_label_color(new_value)


func _set_label_color(value: String) -> void:
	match value:
		"GREEN":
			color_desc.text = green
			color_desc.self_modulate = Global.GREEN
		"YELLOW":
			color_desc.text = yellow
			color_desc.self_modulate = Global.YELLOW
		"RED":
			color_desc.text = red
			color_desc.self_modulate = Global.RED
		"GREY":
			color_desc.text = grey
			color_desc.self_modulate = Global.GREY
		_:
			color_desc.text = ""
			color_desc.self_modulate = Global.WHITE


func _on_Green_pressed() -> void:
	self.value = "GREEN"


func _on_Yellow_pressed() -> void:
	self.value = "YELLOW"


func _on_Red_pressed() -> void:
	self.value = "RED"


func _on_Grey_pressed() -> void:
	self.value = "GREY"


func translation(lang_code: int) -> void:
	match(lang_code):
		LanguageMgt.EN:
			green = "GREEN"
			yellow = "YELLOW"
			red = "RED"
			grey = "GREY"
		LanguageMgt.PL:
			green = "ZIELONY"
			yellow = "ŻÓŁTY"
			red = "CZERWONY"
			grey = "SZARY"
