extends Control


onready var field_list = $Panel/FieldList
onready var label = $LabelColor
onready var window = $Window
onready var special_btn = $Panel/Header/StarBtn


var person: Person = null
var line: Object = null
var lines: Array


func _init() -> void:
	EventMgt.connect("remove_person", self, "_remove_person")


# clear all data from fields
func clear() -> void:
	for field in field_list.get_children():
		field.value = ""
	
	label.color = Global.WHITE
	
	# clear special
	special_btn.modulate = Global.WHITE


# init person card with data
func init(line, lines) -> void:
	self.person = line.get_person()
	self.line = line
	self.lines = lines
	
	clear()
	
	var category = _get_field("Category")
	category.init(DataMgt.category_list, person.category)
	
	var location = _get_field("Location")
	location.init(DataMgt.location_list, person.location)
	
	for field in field_list.get_children():
		field.disable_editable()
	
	print(person.get_data_dict())
	_get_field("Forename").value = person.forename
	_get_field("Location").value = person.location
	_get_field("Contact").value = person.contact
	_get_field("Category").value = person.category
	_get_field("Description").value = person.description
	
	_set_special()
	
	label.value = person.label


# get field from field list
func _get_field(field_name: String) -> Object:
	return field_list.get_node_or_null(field_name)


# switch editable mode for all fields
func _switch_editable() -> void:
	for field in field_list.get_children():
		field.switch_editable()


# sets person to special
func _set_special() -> void:
	if person.special:
		special_btn.modulate = Global.GOLD
	else:
		special_btn.modulate = Global.WHITE


# [EVENT] saves person data
func _save_person_data() -> void:
	person.forename = _get_field("Forename").value
	person.location = _get_field("Location").value
	person.contact = _get_field("Contact").value
	person.category = _get_field("Category").value
	person.description = _get_field("Description").value
	person.label = label.value
	
	DataMgt.save_data()
	line.init(person)
	EventMgt.emit_signal("set_stats", DataMgt.person_list)


# [EVENT] removes person
func _remove_person() -> void:
	line.remove()
	DataMgt.remove_person(person)
	EventMgt.emit_signal("switch_to_main_panel")
	EventMgt.emit_signal("set_stats", DataMgt.person_list)
	DataMgt.save_data()


# [EVENT] delete person and save data
func _on_DeleteBtn_pressed() -> void:
	window.popup_centered()


# [EVENT] switch editable
func _on_EditBtn_pressed() -> void:
	_switch_editable()


# [EVENT] make special
func _on_StarBtn_pressed() -> void:
	_save_person_data()
	person.special = !person.special
	_set_special()


# [EVENT] switch to main panel
func _on_Back_pressed() -> void:
	_save_person_data()
	EventMgt.emit_signal("switch_to_main_panel")


# [EVENT] switch to previous person card 
func _on_SwitchLeft_pressed() -> void:
	_save_person_data()
	var index = lines.find(line)
	if index > 0:
		index -= 1
		var new_line = lines[index]
		init(new_line, lines)


# [EVENT] switch to next person card 
func _on_SwitchRight_pressed() -> void:
	_save_person_data()
	var index = lines.find(line)
	if index < lines.size() - 1:
		index += 1
		var new_line = lines[index]
		init(new_line, lines)


# [EVENT] grab focus on background click
func _on_BG_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton && event.is_pressed():
		var current_focus_control = get_focus_owner()
		if current_focus_control:
			current_focus_control.release_focus()


func translation(lang_code: int) -> void:
	var header = $Panel/Header/Label
	var forename = _get_field("Forename")
	var location = _get_field("Location")
	var contact = _get_field("Contact")
	var category = _get_field("Category")
	var description = _get_field("Description")
	var back = $Panel/Buttons/Back
	
	match(lang_code):
		LanguageMgt.EN:
			header.text = "PERSON CARD"
			forename.text = "Forename"
			location.text = "Location"
			contact.text = "Contact"
			category.text = "Category"
			description.text = "Description"
			back.text = "Back"
		LanguageMgt.PL:
			header.text = "KARTOTEKA OSOBY"
			forename.text = "Imię"
			location.text = "Lokalizacja"
			contact.text = "Kontakt"
			category.text = "Kategoria"
			description.text = "Opis"
			back.text = "Wróć"
