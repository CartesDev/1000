extends Node


const LINE = preload("res://scenes/Line.tscn")
const CATEGORY = preload("res://scenes/Category.tscn")
const LOCATION = preload("res://scenes/Location.tscn")

const SCREEN_SIZE = Vector2(256, 512)

const GREEN = Color(0,1,0)
const YELLOW = Color(1,1,0)
const RED = Color(1,0,0)
const GREY = Color(0.5,0.5,0.5)
const WHITE = Color(1,1,1)
const GOLD = Color(1, 0.843137255, 0)

