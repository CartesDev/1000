# NewRecordPanel
extends Control


onready var field_list = $Panel/FieldList


# initializes panel to create new record
func init() -> void:
	for field in field_list.get_children():
		field.value = ""
		
	var category = _get_field("Category")
	category.init(DataMgt.category_list, "")
	
	var location = _get_field("Location")
	location.init(DataMgt.location_list, "")
	
	grab_focus()


# checks if all of necessary fields have been filled
func _check_validation() -> bool:
	if _get_field("Label").value == "":
		return false
	return true


# gets data from fields and saves to the "data" dictionary
func _get_data_from_fields(data: Dictionary) -> void:
	data.forename = _get_field("Forename").value
	data.location = _get_field("Location").value
	data.contact = _get_field("Contact").value
	data.category = _get_field("Category").value
	data.description = _get_field("Description").value
	data.label = _get_field("Label").value


# gets field from field list 
func _get_field(field_name: String) -> Object:
	return field_list.get_node_or_null(field_name)


# [EVENT] if the field's data has been validated creates new person and saves data
func _on_Accept_pressed() -> void:
	if _check_validation():
		var data := Dictionary()
		_get_data_from_fields(data)
		
		var person = DataMgt.create_person(data.forename, data.location, data.contact, 
			data.category, data.description, data.label, false)
			
		DataMgt.save_data()
		
		EventMgt.emit_signal("create_line", person)
		EventMgt.emit_signal("set_stats", DataMgt.person_list)
		EventMgt.emit_signal("switch_to_main_panel")


# [EVENT] switches to main panel
func _on_Cancel_pressed() -> void:
	EventMgt.emit_signal("switch_to_main_panel")


# [EVENT] releases focus on background click
func _on_NewRecordPanel_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton and event.is_pressed():
		var current_focus_control = get_focus_owner()
		if current_focus_control:
			current_focus_control.release_focus()


func translation(lang_code: int) -> void:
	var header = $Panel/Label
	var forename = _get_field("Forename")
	var location = _get_field("Location")
	var contact = _get_field("Contact")
	var category = _get_field("Category")
	var description = _get_field("Description")
	var label = _get_field("Label")
	var accept = $Panel/Buttons/Accept
	var cancel = $Panel/Buttons/Cancel
	
	match(lang_code):
		LanguageMgt.EN:
			header.text = "ADD NEW RECORD"
			forename.text = "Forename"
			location.text = "Location"
			contact.text = "Contact"
			category.text = "Category"
			description.text = "Description"
			label.text = "Label"
			accept.text = "Accept"
			cancel.text = "Cancel"
		LanguageMgt.PL:
			header.text = "DODAJ NOWY REKORD"
			forename.text = "Imię"
			location.text = "Lokalizacja"
			contact.text = "Kontakt"
			category.text = "Kategoria"
			description.text = "Opis"
			label.text = "Etykieta"
			accept.text = "Potwierdź"
			cancel.text = "Anuluj"
	
	
